# MLOps_ODS
Проект создан для прохождения курса [MLOps и production в DS исследованиях 3.0](https://ods.ai/tracks/mlops3-course-spring-2024)

# Развертывание docker образа

Для создания docker образа с `python` используется `multistage dockerfile`

1. Для создания образа введите команду:

```bash
docker image build -t image_name --target your_stage .
```
для процессоров с архитектурой ARM добавьте флаг `--platform linux/amd64`
```bash
docker image build --platform linux/amd64 -t image_name --target your_stage .
```

где your_stage - стадия сборки.
на текущий момент в проекте три стадии сборки:
- builder
- dev
- prod

2. Для запуска контейнера введите команду:
```bash
docker run --name container_name image_name
```
например:
```bash
docker run --name dev dev
```
- Для автоматического удаления контейнера после выключения добавьте флаг `--rm`
```bash
    docker run --rm --name dev dev
```
- Для запуска с подключением консоли `python` добавьте флаг `--it`
```bash
    docker run --rm --it --name dev dev
```
- Для проброса портов добавьте флаг `-p docker_port:container_port`
```bash
    docker run --rm --it -p 8080:8080 --name dev dev
```
- Для проброса папки добавьте флаг `-v local_dir:container_dir`
```bash
    docker run --rm -it -p 8080:8080 -v ./src:/mlops_ods/src --name dev dev
```
3. Для запуска сервера в контейнере введите:
```bash
    docker exec container_name pdm run run_server_command
```

+ например команды для запуска backend сервера:

    1. Создание образа
    ```bash
        docker image build -t dev --target dev .
    ```

    2. Запуск контейнера
    ```bash
        docker run --rm -it -p 8080:8080 -v ./src/backend:/mlops_ods/src/backend --name backend dev
    ```

    3. Запуск сервера `uvicorn`
    ```bash
        docker exec backend pdm run uvicorn src.backend.backend:app --host 0.0.0.0 --port 8080 --reload
    ```
+ например команды для запуска frontend сервера:

    1. Создание образа
    ```bash
        docker image build -t dev --target dev .
    ```

    2. Запуск контейнера
    ```bash
        docker run --rm -it -p 8501:8501 -v ./src/frontend:/mlops_ods/src/frontend --name frontend dev
    ```

    3. Запуск сервера `streamlit`
    ```bash
        docker exec frontend pdm run streamlit run src/frontend/main.py --server.port=8501
    ```
+ например команды для запуска jupyterLab сервера:

    1. Создание образа
    ```bash
        docker image build -t dev --target dev .
    ```

    2. Запуск контейнера
    ```bash
        docker run --rm -it -p 8889:8889 -v ./src/notebook:/mlops_ods/notebook --name jupiterLab dev
    ```

    3. Запуск сервера `jupyter`
    ```bash
        docker exec jupiterLab jupyter notebook --ip 0.0.0.0 --port 8889 --allow-root
    ```
    Скопируйте токен из консоли

# Использование образа для snakemake

+ например команды для запуска jupyterLab сервера:

    1. Создание образа
    ```bash
        docker image build -t mamba --target research .
    ```

    2. Запуск контейнера
    ```bash
        docker run --rm -it -v ./src/notebook:/mlops_ods/notebook --name snakemake mamba
    ```

    3. Запуск `snakemake`
    ```bash
        cd notebook
        snakemake --cores all
    ```
    Результаты работы `snakemake` отобразяться в папке `logs/notebooks`


# Документация

Документация проекта генерируется с помощью quarto-cli и публикуется на gitlab pages

Документация хранится в папке ```docs```

+ например команды для запуска Quarto сервера:

    1. Создание образа
    ```bash
        docker image build -t dev --target dev .
    ```

    2. Запуск контейнера
    ```bash
        docker run --rm -it -p 4591:4591 -v ./src:/mlops_ods/src --name quarto dev
    ```
    3. Генерация документации
    ```bash
        docker exec quarto pdm run quarto render src/docs
    ```

    4. Запуск сервера `quarto`
    ```bash
        docker exec quarto pdm run quarto preview src/docs
    ```





# Contributing
[contributing](contributing.md)
