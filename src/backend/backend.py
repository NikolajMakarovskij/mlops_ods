"""
root of backend
Returns:
    _type_: _description_
"""

from fastapi import FastAPI

app = FastAPI()


@app.get("/")
async def root():
    """
    backend homepage
    Returns:
        type: str
        description: message
    """
    return {"message": "backend root"}
