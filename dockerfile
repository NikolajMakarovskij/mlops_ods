ARG PYTHON_BASE=3.12-slim
#run research stage
FROM mambaorg/micromamba:1-jammy-cuda-12.4.1 AS research
WORKDIR /mlops_ods
COPY pyproject.toml pdm.lock README.md .dockerignore LICENSE /mlops_ods/
COPY --chown=$MAMBA_USER:$MAMBA_USER env.yaml /tmp/env.yaml
RUN micromamba install -y -n base -f /tmp/env.yaml && \
    micromamba clean --all --yes
# build stage
FROM python:$PYTHON_BASE AS builder
# build base image
RUN pip install -U pdm
ENV PDM_CHECK_UODATE=false
# install dependencies
WORKDIR /mlops_ods
COPY pyproject.toml pdm.lock README.md .dockerignore LICENSE /mlops_ods/
RUN pdm lock

# run dev stage
FROM builder AS dev
# add git folder for isort check
RUN apt-get update && \
    apt-get install git-all -y
COPY .git/ /mlops_ods/.git/
# retrieve packages from build stage
COPY --from=builder /mlops_ods/.venv /mlops_ods/.venv
RUN pdm install --check --dev

ENV PATH="/mlops_ods/.venv/bin:$PATH"

# run prod stage
FROM builder AS prod

# retrieve packages from build stage
COPY --from=builder /mlops_ods/.venv /mlops_ods/.venv
RUN pdm install --check --prod --no-editable
COPY ./src /mlops_ods/src

ENV PATH="/mlops_ods/.venv/bin:$PATH"
